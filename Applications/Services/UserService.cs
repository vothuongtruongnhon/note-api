﻿using Applications.InterfaceServices;
using Applications.Utils;
using Applications.ViewModels.UserViewModels;
using Domain.Entities;
using MapsterMapper;
using Microsoft.Extensions.Configuration;

namespace Applications.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTimeService _timeService;
        private readonly IConfiguration _configuration;

        public UserService(IUnitOfWork unitOfWork,
            IMapper mapper,
            ICurrentTimeService timeService,
            IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _timeService = timeService;
            _configuration = configuration;
        }

        public async Task<List<User>> GetUsersAsync() => await _unitOfWork.UserRepository.GetAllAsync();

        public async Task<string> LoginAsync(LoginUserViewModel user)
        {
            var login = await _unitOfWork.UserRepository.FindUserByUserName(user.UserName);
            // verify passwordHash
            if (!BCrypt.Net.BCrypt.Verify(user.PassWord, login.PassWord))
            {
                return "Incorrect Password!!!";
            }

            return login.GenerateJWT(_configuration);
        }

        public async Task<string> RegisterAsync(RegisterUserViewModel user)
        {
            // check userName is exist or not
            var isExist = await _unitOfWork.UserRepository.ExistedUser(user.UserName);

            if (isExist)
            {
                return "UserName already Exist!!!";
            }

            // create new User
            var newUser = new User
            {
                UserName = user.UserName,
                PassWord = user.PassWord.Hash(),
                Name = user.Name,
                ImageURL = user.ImageURL,
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);
            await _unitOfWork.SaveChangeAsync();
            return "Create User Success";
        }
    }
}
