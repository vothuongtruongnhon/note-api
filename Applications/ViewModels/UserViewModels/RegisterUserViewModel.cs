﻿namespace Applications.ViewModels.UserViewModels
{
    public class RegisterUserViewModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
    }
}
