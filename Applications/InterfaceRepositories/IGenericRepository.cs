﻿using Applications.Common;
using Domain.BaseEntity;

namespace Applications.InterfaceRepositories
{
    public interface IGenericRepository<TEntity> where TEntity : Base
    {
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity?> GetById(int Id);
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        void SoftDelete(TEntity entity);
        Task AddRangeAsync(List<TEntity> entities);
        void UpdateRange(List<TEntity> entities);
        void SoftDeleteRange(List<TEntity> entities);
        Task<Pagination<TEntity>> PaginationAsync(int pageIndex = 0, int pageSize = 10);
    }
}
