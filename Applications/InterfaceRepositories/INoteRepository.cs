﻿using Domain.Entities;

namespace Applications.InterfaceRepositories
{
    public interface INoteRepository : IGenericRepository<Note>
    {
    }
}
