﻿namespace Applications.InterfaceServices
{
    public interface IClaimService
    {
        public int GetCurrentUserId { get; }
    }
}
