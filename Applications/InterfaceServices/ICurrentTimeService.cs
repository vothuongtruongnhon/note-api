﻿namespace Applications.InterfaceServices
{
    public interface ICurrentTimeService
    {
        public DateTime GetCurrentTime();
    }
}
