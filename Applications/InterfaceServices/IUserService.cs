﻿using Applications.ViewModels.UserViewModels;
using Domain.Entities;

namespace Applications.InterfaceServices
{
    public interface IUserService
    {
        public Task<string> RegisterAsync(RegisterUserViewModel user);
        public Task<string> LoginAsync(LoginUserViewModel user);
        public Task<List<User>> GetUsersAsync();
    }
}
