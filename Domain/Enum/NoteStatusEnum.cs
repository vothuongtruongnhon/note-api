﻿namespace Domain.Enum
{
    public enum NoteStatusEnum
    {
        Doing,
        Done
    }
}
