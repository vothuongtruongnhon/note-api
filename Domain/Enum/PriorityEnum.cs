﻿namespace Domain.Enum
{
    public enum PriorityEnum
    {
        Unknow,
        LowPriority,
        Neutral,
        HighPriority,
        Critical,
    }
}
