﻿using Applications;
using Applications.Common;
using Applications.InterfaceRepositories;
using Applications.InterfaceServices;
using Applications.Services;
using Infrastructures.Repositories;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Infrastructures
{
    public static class DependencyInjections
    {
        public static IServiceCollection InfrastructureServices(this IServiceCollection services, IConfiguration config)
        {
            // injection interfaces and it implementation
            {
                services.AddScoped<IUnitOfWork, UnitOfWork>();
                services.AddScoped<IUserRepository, UserRepository>();
                services.AddScoped<INoteRepository, NoteRepository>();
                services.AddSingleton<ICurrentTimeService, CurrentTimeService>();
                services.AddScoped<IUserService, UserService>();
            }

            // injection config Mapster
            {
                // scan for mapster config
                var configMapster = TypeAdapterConfig.GlobalSettings;
                configMapster.Scan(Assembly.GetExecutingAssembly());
                // add injection
                services.AddSingleton<IMapper, Mapper>();
            }

            // injection DB
            services.AddDbContext<AppDbContext>(op => op.UseSqlServer(config.GetConnectionString("NoteDB")));

            return services;
        }
    }
}
