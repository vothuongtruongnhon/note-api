﻿using Applications.InterfaceRepositories;
using Applications.InterfaceServices;
using Domain.Entities;

namespace Infrastructures.Repositories
{
    public class NoteRepository : GenericRepository<Note>, INoteRepository
    {
        private readonly AppDbContext _context;
        public NoteRepository(AppDbContext context,
            ICurrentTimeService currentTime,
            IClaimService claimService) :
            base(context, currentTime, claimService)
        {
            _context = context;
        }
    }
}
