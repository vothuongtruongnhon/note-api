﻿using Applications.Common;
using Applications.InterfaceRepositories;
using Applications.InterfaceServices;
using Domain.BaseEntity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : Base
    {
        private DbSet<TEntity> _dbSet;
        private ICurrentTimeService _currentTime;
        private IClaimService _claimService;
        public GenericRepository(AppDbContext context, ICurrentTimeService currentTime, IClaimService claimService)
        {
            _dbSet = context.Set<TEntity>();
            _currentTime = currentTime;
            _claimService = claimService;
        }

        public async Task AddAsync(TEntity entity)
        {
            entity.CreationDate = _currentTime.GetCurrentTime();
            entity.CreatedBy = _claimService.GetCurrentUserId;

            await _dbSet.AddAsync(entity);
        }

        public async Task AddRangeAsync(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreationDate = _currentTime.GetCurrentTime();
                entity.CreatedBy = _claimService.GetCurrentUserId;
            }

            await _dbSet.AddRangeAsync(entities);
        }

        public async Task<List<TEntity>> GetAllAsync() => await _dbSet.ToListAsync();

        public async Task<TEntity?> GetById(int Id) => await _dbSet.Where(x => x.Id == Id).FirstOrDefaultAsync();

        public async Task<Pagination<TEntity>> PaginationAsync(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<TEntity>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                Items = items
            };

            return result;
        }

        public void SoftDelete(TEntity entity)
        {
            entity.DeletionDate = _currentTime.GetCurrentTime();
            entity.DeletedBy = _claimService.GetCurrentUserId;
            entity.IsDeleted = true;

            _dbSet.Update(entity);
        }

        public void SoftDeleteRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.DeletionDate = _currentTime.GetCurrentTime();
                entity.DeletedBy = _claimService.GetCurrentUserId;
                entity.IsDeleted = true;
            }

            _dbSet.UpdateRange(entities);
        }

        public void Update(TEntity entity)
        {
            entity.ModificationDate = _currentTime.GetCurrentTime();
            entity.ModificatedBy = _claimService.GetCurrentUserId;

            _dbSet.Update(entity);
        }

        public void UpdateRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.ModificationDate = _currentTime.GetCurrentTime();
                entity.ModificatedBy = _claimService.GetCurrentUserId;
            }

            _dbSet.UpdateRange(entities);
        }
    }
}
