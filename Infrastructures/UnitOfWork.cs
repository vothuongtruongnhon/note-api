﻿using Applications;
using Applications.InterfaceRepositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IUserRepository _userRepository;
        private readonly INoteRepository _noteRepository;
        public UnitOfWork(AppDbContext context,
            IUserRepository userRepository,
            INoteRepository noteRepository)
        {
            _context = context;
            _userRepository = userRepository;
            _noteRepository = noteRepository;
        }

        public INoteRepository NoteRepository => _noteRepository;

        public IUserRepository UserRepository => _userRepository;

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();
    }
}
