﻿using APIs.Services;
using APIs.Validations;
using Applications.InterfaceServices;
using Applications.ViewModels.UserViewModels;
using FluentValidation;
using FluentValidation.AspNetCore;

namespace APIs
{
    public static class DependencyInjection
    {
        public static IServiceCollection APIServices(this IServiceCollection services)
        {
            // support for claim
            {
                services.AddScoped<IClaimService, ClaimService>();
                services.AddHttpContextAccessor();
            }

            // defaul
            {
                services.AddControllers();
                services.AddEndpointsApiExplorer();
                services.AddSwaggerGen();
            }

            // validation
            {
                // server side
                services.AddFluentValidationAutoValidation();
                services.AddFluentValidationClientsideAdapters();
                // client side
                services.AddScoped<IValidator<LoginUserViewModel>, LoginValidation>();
                services.AddScoped<IValidator<RegisterUserViewModel>, RegisterValidation>();
                services.AddScoped<IValidator<UpdateUserViewModel>, UpdateUserValidation>();
            }

            return services;
        }
    }
}
