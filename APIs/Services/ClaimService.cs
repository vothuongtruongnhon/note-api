﻿using Applications.InterfaceServices;
using System.Security.Claims;

namespace APIs.Services
{
    public class ClaimService : IClaimService
    {
        public ClaimService(IHttpContextAccessor httpContextAccessor)
        {
            var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("userId");
            GetCurrentUserId = string.IsNullOrEmpty(Id) ? -1 : int.Parse(Id);
        }
        public int GetCurrentUserId { get; }
    }
}
